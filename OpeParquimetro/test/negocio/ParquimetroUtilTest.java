/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package negocio;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import negocio.util.ParquimetroUtil;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import park.domain.Moeda;
import park.domain.Parquimetro;

/**
 *
 * @author Leonardo
 */
public class ParquimetroUtilTest {

    Parquimetro parquimetro;

    public ParquimetroUtilTest() {
    }

    @BeforeClass
    public static void setUpClass() {

    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        parquimetro = new Parquimetro(1, "Luis Fontoura júnior", "06:00:00", "18:00:00", 30, 120, (float) 0.25, 10);
        List<Moeda> moedas = new ArrayList<Moeda>();
        moedas.add(Moeda.UM_REAL);
        moedas.add(Moeda.CINQUENTA_CENTAVOS);
        moedas.add(Moeda.DEZ_CENTAVOS);
        parquimetro.setMoedasPermitidas(moedas);
    }

    @After
    public void tearDown() {
    }

    @Test
    public void getMinutosPorValor() {
        assertEquals(ParquimetroUtil.getMinutosPorValor(parquimetro, (float) 1.25), 50);
    }

    @Test
    public void getMinutosPorValorMoedas() {
        List<Moeda> moedasInseridas = new ArrayList<Moeda>();
        moedasInseridas.add(Moeda.UM_REAL);
        moedasInseridas.add(Moeda.UM_CENTAVO);
        moedasInseridas.add(Moeda.DEZ_CENTAVOS);
        moedasInseridas.add(Moeda.CINQUENTA_CENTAVOS);
        moedasInseridas.add(Moeda.VINTE_E_CINCO_CENTAVOS);
        assertEquals(ParquimetroUtil.getMinutosPorValor(parquimetro, moedasInseridas), 70);
    }

    @Test
    public void getValorTotal() {
        List<Moeda> moedasInseridas = new ArrayList<Moeda>();
        moedasInseridas.add(Moeda.UM_REAL);
        moedasInseridas.add(Moeda.UM_CENTAVO);
        moedasInseridas.add(Moeda.DEZ_CENTAVOS);
        moedasInseridas.add(Moeda.CINQUENTA_CENTAVOS);
        moedasInseridas.add(Moeda.VINTE_E_CINCO_CENTAVOS);
        assertEquals(ParquimetroUtil.getValorTotal(parquimetro, moedasInseridas), 1.75, 0.0f);
    }

    @Test
    public void getValorMaximo() {
        assertEquals(ParquimetroUtil.getValorMaximo(parquimetro), (float) 3, 0.0f);
    }

    @Test
    public void getValorMinimo() {
        assertEquals(ParquimetroUtil.getValorMinimo(parquimetro), (float) 0.75, 0.0f);
    }

    @Test
    public void getValidade() {
        Calendar c = Calendar.getInstance();
        Date dataEmissao = new Date();
        c.setTime(dataEmissao);
        c.add(Calendar.MINUTE, 10);
        assertEquals(ParquimetroUtil.getValidade(dataEmissao, 10), c.getTime());
    }

    @Test
    public void getTroco() {
        List<Moeda> moedasInseridas = new ArrayList<Moeda>();
        moedasInseridas.add(Moeda.DEZ_CENTAVOS);
        moedasInseridas.add(Moeda.UM_REAL);
        moedasInseridas.add(Moeda.UM_CENTAVO);
        moedasInseridas.add(Moeda.CINCO_CENTAVOS);
        moedasInseridas.add(Moeda.CINQUENTA_CENTAVOS);
        moedasInseridas.add(Moeda.VINTE_E_CINCO_CENTAVOS);
        moedasInseridas.add(Moeda.UM_REAL);
        moedasInseridas.add(Moeda.UM_CENTAVO);
        moedasInseridas.add(Moeda.UM_CENTAVO);        

        List<Moeda> moedasTroco = new ArrayList<Moeda>();
        moedasTroco.add(Moeda.UM_REAL);
        moedasTroco.add(Moeda.CINQUENTA_CENTAVOS);
        moedasTroco.add(Moeda.VINTE_E_CINCO_CENTAVOS);
        moedasTroco.add(Moeda.DEZ_CENTAVOS);
        moedasTroco.add(Moeda.UM_CENTAVO);        
        assertEquals(ParquimetroUtil.getTroco(moedasInseridas, (float) 1.86), moedasTroco);
    }
}
