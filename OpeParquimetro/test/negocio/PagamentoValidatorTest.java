/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package negocio;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Leonardo
 */
public class PagamentoValidatorTest {
    
    public PagamentoValidatorTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void validaCartaoRecarregavel(){
        assertFalse(PagamentoValidator.validaCartaoRecarregavel("asdasasdasd"));
    }
    @Test
    public void validaCartaoIsento(){
        assertFalse(PagamentoValidator.validaCartaoIsento("12312312"));
    }
    
    
}
