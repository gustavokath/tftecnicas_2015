/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package negocio;

import java.util.ArrayList;
import java.util.List;
import negocio.util.ParquimetroUtil;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import park.domain.Moeda;
import park.domain.Parquimetro;

/**
 *
 * @author Leonardo
 */
public class ParquimetroValidatorTest {

    Parquimetro parquimetro;

    public ParquimetroValidatorTest() {
    }

    @BeforeClass
    public static void setUpClass() {

    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        parquimetro = new Parquimetro(1, "Luis Fontoura júnior", "06:00:00", "18:00:00", 30, 120, (float) 0.25, 10);
        List<Moeda> moedas = new ArrayList<Moeda>();
        moedas.add(Moeda.UM_REAL);
        moedas.add(Moeda.CINQUENTA_CENTAVOS);
        moedas.add(Moeda.DEZ_CENTAVOS);
        parquimetro.setMoedasPermitidas(moedas);
    }

    @After
    public void tearDown() {
    }

    @Test
    public void validaIdentificador() {
        assertFalse(ParquimetroValidator.validaIdentificador("123"));
    }

    @Test
    public void validaValor() {
        assertTrue(ParquimetroValidator.validaValor(parquimetro, 4));
    }

    @Test
    public void validaValorCartao() {
        assertFalse(ParquimetroValidator.validaValorCartao(parquimetro, (float) 3.1));
    }
}
