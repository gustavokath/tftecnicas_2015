/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import negocio.JsonException;
import negocio.MoedaException;
import negocio.PagamentoException;
import park.domain.Moeda;
import park.domain.Parquimetro;

/**
 *
 * @author Leonardo
 */
public class Main {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        Parquimetro parquimetro = new Parquimetro(1, "Luis Fontoura júnior", "06:00:00", "18:00:00", 30, 120, (float) 0.25, 10);
        List<Moeda> moedas = new ArrayList<Moeda>();
        moedas.add(Moeda.UM_REAL);
        moedas.add(Moeda.CINQUENTA_CENTAVOS);
        moedas.add(Moeda.DEZ_CENTAVOS);
        parquimetro.setMoedasPermitidas(moedas);
        ParquimetroController controller = new ParquimetroController(parquimetro);
        while (true) {
            System.out.println("Menu:");
            System.out.println("1 - inserir moeda");
            System.out.println("2 - cancelar operação");
            System.out.println("3 - concluir operação");
            System.out.println("4 - incrementar validade");
            System.out.println("5 - decrementar validade");
            System.out.println("0 - sair");
            System.out.println("Valor total de moedas:" + controller.getValorTotalMoedasInseridas());
            int op = scan.nextInt();
            if (op == 0) {
                break;
            }
            switch (op) {
                case 1:
                    insereMoeda(controller);
                    break;
                case 2:
                    cancelaOperacao(controller);
                    break;
                case 3:
                    concluiOperacao(controller);
                    break;
                case 4:
                    incrementaValidade(controller);
                    break;
                case 5:
                    decrementaValidade(controller);
                    break;
                case 6:
                    admin(controller);
                    break;
            }
        }
    }

    public static void insereMoeda(ParquimetroController controller) {
        while (true) {
            Scanner scan = new Scanner(System.in);
            System.out.println("Selecione uma moeda:");
            System.out.println("1 - 1 REAL");
            System.out.println("2 - 50");
            System.out.println("3 - 25");
            System.out.println("4 - 10");
            System.out.println("5 - 5");
            System.out.println("6 - 1");
            System.out.println("0 - Sair");
            System.out.println("Valor total de moedas:" + controller.getValorTotalMoedasInseridas());
            int op = scan.nextInt();
            if (op == 0) {
                break;
            }
            try {
                Moeda m = null;
                switch (op) {
                    case 1:
                        m = Moeda.UM_REAL;                        
                        break;
                    case 2:
                        m = Moeda.CINQUENTA_CENTAVOS;
                        break;
                    case 3:
                        m = Moeda.VINTE_E_CINCO_CENTAVOS;
                        break;
                    case 4:
                        m = Moeda.DEZ_CENTAVOS;
                        break;
                    case 5:
                        m = Moeda.CINCO_CENTAVOS;
                        break;
                    case 6:
                        m = Moeda.UM_CENTAVO;
                        break;
                }
                controller.insereMoeda(m);
                System.out.println("Moeda inserida");
            } catch (MoedaException m) {
                System.out.println(m.getMessage());
            }
        }
    }

    public static void cancelaOperacao(ParquimetroController controller) {
        controller.cancelaOperacao();
        System.out.println("Cancelamento efetuado");
    }

    public static void concluiOperacao(ParquimetroController controller) {
        Scanner scan = new Scanner(System.in);
        while (true) {
            System.out.println("Conclui Operação:");
            System.out.println("1 - pagamento moeda");
            System.out.println("2 - pagamento isento");
            System.out.println("3 - pagamento recarregável");
            System.out.println("0 - sair");
            int op = scan.nextInt();
            if (op == 0) {
                break;
            }
            controller.setTipoDePagamento(op);
            if(op == 2 || op == 3){
                System.out.println("Informe o cartão");
                scan.nextLine();
                String cartao = scan.nextLine();
                controller.setCartao(cartao);
                System.out.println("Informe o valor");
                float valor = scan.nextFloat();
                controller.setValor(valor);
            }
            try {
                controller.concluiOperacao();
                System.out.println("Pagamento efetuado");
                break;
            } catch (PagamentoException ex) {
                System.out.println("Problema ao efetuar o pagamento.\n" + ex.getMessage());
            } catch (JsonException ex) {
                System.out.println("Problema ao gerar o arquivo.\n" + ex.getMessage());
            } catch (IOException ex) {
                System.out.println("Problema ao gerar o arquivo.");
            }
        }        
    }

    public static void incrementaValidade(ParquimetroController controller) {
        controller.incremento();
        System.out.println("Incremento efetuado");
    }

    public static void decrementaValidade(ParquimetroController controller) {
        controller.decremento();
        System.out.println("Decremento efetuado");
    }
    
    public static void admin(ParquimetroController controller){
        System.out.println("valor total de moedas:" + controller.getValorTotalMoedasParquimetro());
    }
}