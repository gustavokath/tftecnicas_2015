/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import negocio.JsonException;
import negocio.MoedaException;
import negocio.MoedaValidator;
import negocio.PagamentoException;
import negocio.ParquimetroFacade;
import negocio.ParquimetroValidator;
import negocio.util.ParquimetroUtil;
import park.domain.Moeda;
import park.domain.Parquimetro;
import park.domain.Ticket;

/**
 *
 * @author Leonardo
 */
public class ParquimetroController {

    private List<Moeda> moedas;
    private List<Moeda> moedasInseridas;
    private int incremento;
    private float valor;
    private String cartao;
    private Parquimetro parquimetro;
    private ParquimetroFacade parquimetroFacade;
    private int tipoDePagamento;

    public ParquimetroController(Parquimetro parquimetro) {
        this.moedasInseridas = new ArrayList<Moeda>();
        this.moedas = new ArrayList<Moeda>();
        this.parquimetro = parquimetro;
        this.parquimetroFacade = new ParquimetroFacade(parquimetro);
        this.incremento = 1;
        this.tipoDePagamento = 0;
    }

    public void incremento() {
        this.incremento++;
    }

    public void decremento() {
        if (this.incremento > 0) {
            this.incremento--;
        }
    }

    public float getValor() {
        return valor;
    }

    public void setValor(float valor) {
        this.valor = valor;
    }

    public void cancelaOperacao() {
        if (tipoDePagamento == 1) {
            ParquimetroUtil.devolveMoedas(moedas, moedasInseridas);
        }
        incremento = 0;
        cartao = "";
        moedasInseridas = new ArrayList<Moeda>();
        tipoDePagamento = 0;
    }

    public String getCartao() {
        return cartao;
    }

    public void setCartao(String cartao) {
        this.cartao = cartao;
    }

    public void sendMessageToView(String message) {
        System.out.println(message);
    }

    public void concluiOperacao() throws PagamentoException, JsonException, IOException {
        //usuário clica no botão verde
        Ticket ticket = null;
        if (tipoDePagamento == 1) {
            float valorTotalInserido = ParquimetroUtil.getValorTotalByMoedas(moedasInseridas);
            if (ParquimetroValidator.validaValor(parquimetro, valorTotalInserido)) {
                ticket = this.pagarComMoeda();
                if (ticket.getPagamento().getValor() < valorTotalInserido) {
                    List<Moeda> moedasTroco = ParquimetroUtil.getTroco(moedas, valorTotalInserido - ticket.getPagamento().getValor());
                    sendMessageToView("Seu troco é:" + ParquimetroUtil.getValorTotalByMoedas(moedasTroco));
                    ParquimetroUtil.devolveMoedas(moedas, moedasInseridas);
                }
                moedasInseridas = new ArrayList<Moeda>();
            } else {
                throw new PagamentoException("Quantidade de moedas inseridas insuficiente");
            }
        } else {
            if (!ParquimetroValidator.validaValorCartao(parquimetro, valor)) {
                valor = 0;
                throw new PagamentoException("Valor inválido");
            }
            if (tipoDePagamento == 2) {
                ticket = this.pagarComIsento(valor, cartao);
            } else if (tipoDePagamento == 3) {
                ticket = this.pagarComRecarregavel(valor, cartao);
            }
            cartao = "";
            valor = 0;
        }
        ParquimetroUtil.generateTicketJson(ticket);
    }

    public void insereMoeda(Moeda moeda) throws MoedaException {
        if (MoedaValidator.validaPorTipoMoeda(parquimetro.getMoedasPermitidas(), moeda)) {
            this.moedasInseridas.add(moeda);
            this.moedas.add(moeda);
        } else {
            throw new MoedaException("Moeda inválida");
        }
    }

    public Ticket pagarComMoeda() {
        return this.parquimetroFacade.pagarTicketComMoeda(moedasInseridas);
    }

    public Ticket pagarComIsento(float valor, String cartao) throws PagamentoException {
        return this.parquimetroFacade.pagarTicketComIsento(valor, cartao);
    }

    public Ticket pagarComRecarregavel(float valor, String cartao) throws PagamentoException {
        return this.parquimetroFacade.pagarTicketComRecarregavel(valor, cartao);
    }

    public int getTipoDePagamento() {
        return tipoDePagamento;
    }

    public void setTipoDePagamento(int tipoDePagamento) {
        this.tipoDePagamento = tipoDePagamento;
    }

    public float getValorTotalMoedasInseridas() {
        return ParquimetroUtil.getValorTotalByMoedas(moedasInseridas);
    }

    public float getValorTotalMoedasParquimetro() {
        return ParquimetroUtil.getValorTotalByMoedas(moedas);
    }
}
