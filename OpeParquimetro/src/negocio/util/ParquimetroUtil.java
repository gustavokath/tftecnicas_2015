/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package negocio.util;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Scanner;
import negocio.JsonException;
import negocio.domain.PagamentoIsento;
import negocio.domain.PagamentoRecarregavel;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import park.domain.Moeda;
import park.domain.Parquimetro;
import park.domain.Ticket;

/**
 *
 * @author Leonardo
 */
public class ParquimetroUtil {

    public static Date getValidade(Date dataEmissao, int minutos) {
        Calendar c = Calendar.getInstance();
        c.setTime(dataEmissao);
        c.add(Calendar.MINUTE, minutos);
        dataEmissao = c.getTime();
        return dataEmissao;
    }

    public static int getMinutosPorValor(Parquimetro parquimetro, float valor) {
        int minutos = 0;
        Float f = (float) valor / (float) parquimetro.getValorIncremento();
        minutos = f.intValue() * parquimetro.getIncremento();
        return minutos;
    }

    public static int getMinutosPorValor(Parquimetro parquimetro, List<Moeda> moedasInseridas) {
        int minutos = 0;
        float valor = 0;
        for (Moeda moeda : moedasInseridas) {
            valor += moeda.getNumVal();
            while (valor >= parquimetro.getValorIncremento()) {
                minutos += parquimetro.getIncremento();
                valor -= parquimetro.getValorIncremento();
            }
        }
        return minutos;
    }

    public static float getValorTotal(Parquimetro parquimetro, List<Moeda> moedasInseridas) {
        float valor = 0, valorTotal = 0;
        for (Moeda moeda : moedasInseridas) {
            valor += moeda.getNumVal();
            while (valor >= parquimetro.getValorIncremento()) {
                valorTotal += parquimetro.getValorIncremento();
                valor -= parquimetro.getValorIncremento();
            }
        }
        return valorTotal;
    }

    public static List<Moeda> getTroco(List<Moeda> moedas, float troco) {
        List<Moeda> moedasTroco = new ArrayList<Moeda>();

        Collections.sort(moedas, new Comparator<Moeda>() {
            @Override
            public int compare(Moeda m1, Moeda m2) {
                int retorno = 0;
                if (m1.getNumVal() > m2.getNumVal()) {
                    retorno = -1;
                }
                if (m1.getNumVal() < m2.getNumVal()) {
                    retorno = 1;
                }
                return retorno;
            }
        });

        for(Moeda m : moedas){
            if(m.getNumVal() <= troco){
                moedasTroco.add(m);
                troco -= m.getNumVal();
            }
        }        
        return moedasTroco;
    }

    public static void devolveMoedas(List<Moeda> moedas, List<Moeda> moedasInseridas) {
        for (Moeda moeda : moedasInseridas) {
            moedas.remove(moeda);
        }
    }

    public static void generateTicketJson(Ticket ticket) throws JsonException, IOException {
        try {
            List<JSONObject> list = new ArrayList<JSONObject>();
            JSONObject jsonObj = new JSONObject();
            jsonObj.put("serial", String.format("%05d", ticket.getSerial()));
            jsonObj.put("dataHoraEmissao", ticket.getDataHoraEmissao());
            jsonObj.put("dataHoraValidade", ticket.getDataHoraValidade());
            JSONObject parquimetro = new JSONObject();
            parquimetro.put("endereco", ticket.getParquimetro().getEndereco());
            parquimetro.put("identificacao", String.format("%05d", ticket.getParquimetro().getIdentificacao()));
            parquimetro.put("horarioIni", ticket.getParquimetro().getHorarioIniTarifacao());
            parquimetro.put("horarioFim", ticket.getParquimetro().getHorarioFimTarifacao());
            parquimetro.put("tempoMin", ticket.getParquimetro().getTempoMinimo());
            parquimetro.put("tempoMax", ticket.getParquimetro().getTempoMaximo());
            parquimetro.put("incremento", ticket.getParquimetro().getIncremento());
            parquimetro.put("valorIncremento", ticket.getParquimetro().getValorIncremento());
            parquimetro.put("identificacao_parquimetro", ticket.getParquimetro().getIdentificacao());
            jsonObj.put("parquimetro", parquimetro);
            JSONObject pagamento = new JSONObject();
            pagamento.put("valor", ticket.getPagamento().getValor());
            pagamento.put("dataHora", ticket.getPagamento().getDataHora());
           // pagamento.put("identificacao_pagamento", )
            if (ticket.getPagamento() instanceof PagamentoRecarregavel) {
                pagamento.put("cartao", ((PagamentoRecarregavel) ticket.getPagamento()).getCartao());
            } else if (ticket.getPagamento() instanceof PagamentoIsento) {
                pagamento.put("cartao_recarregavel", ((PagamentoIsento) ticket.getPagamento()).getCartao());
            }
            jsonObj.put("pagamento", pagamento);
            list.add(jsonObj);

            File arquivo = new File("tickets.json");
            if (arquivo.exists()) {
                String content = "";
                try {
                    content = new Scanner(arquivo).useDelimiter("\\Z").next();
                } catch (Exception e) {

                }
                JSONArray tickets;
                if (!content.isEmpty()) {
                    tickets = new JSONArray(content);
                } else {
                    tickets = new JSONArray();
                }
                tickets.put(list);
                try (FileWriter file = new FileWriter("tickets.json")) {
                    file.write(tickets.toString());
                }
            } else {
                JSONArray tickets = new JSONArray();
                tickets.put(list);
                try (FileWriter file = new FileWriter(arquivo)) {
                    file.write(tickets.toString());
                }
            }
        } catch (IOException e) {
            throw new IOException("Erro ao gerar o arquivo");
        } catch (JSONException e) {
            throw new JsonException("Erro ao gerar o arquivo");
        }
        
    }

    public static float getValorMaximo(Parquimetro p) {
        return (p.getTempoMaximo() / p.getIncremento()) * p.getValorIncremento();
    }

    public static float getValorMinimo(Parquimetro p) {
        return (p.getTempoMinimo() / p.getIncremento()) * p.getValorIncremento();
    }

    public static float getValorTotalByMoedas(List<Moeda> moedas) {
        float total = 0;
        for (Moeda moeda : moedas) {
            total += moeda.getNumVal();
        }
        return total;
    }
}
