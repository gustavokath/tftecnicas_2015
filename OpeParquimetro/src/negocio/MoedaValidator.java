/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package negocio;

import park.domain.Moeda;
import java.util.List;

/**
 *
 * @author Leonardo
 */
public class MoedaValidator {
    public static boolean validaPorTipoMoeda(List<Moeda> tiposMoeda, Moeda moeda){
        return tiposMoeda.contains(moeda);
    }
}
