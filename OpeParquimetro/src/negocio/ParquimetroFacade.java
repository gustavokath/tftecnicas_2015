/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package negocio;

import java.time.Instant;
import java.util.Date;
import java.util.List;
import negocio.domain.PagamentoIsento;
import negocio.domain.PagamentoMoeda;
import negocio.domain.PagamentoRecarregavel;
import negocio.util.ParquimetroUtil;
import park.domain.Moeda;
import park.domain.Pagamento;
import park.domain.Parquimetro;
import park.domain.Ticket;

/**
 *
 * @author 14104947
 */
public class ParquimetroFacade {

    private Parquimetro parquimetro;
    private int ticketSerial;

    public ParquimetroFacade(Parquimetro parquimetro) {
        this.parquimetro = parquimetro;
        ticketSerial = 1;
    }

    public Ticket pagarTicketComMoeda(List<Moeda> moedas) {
        float valor = ParquimetroUtil.getValorTotal(parquimetro, moedas);
        Pagamento pagamento = new PagamentoMoeda(valor);
        return gerarTicketMoeda(pagamento, moedas);
    }

    public Ticket pagarTicketComIsento(float valor, String cartao) throws PagamentoException {
        if(!PagamentoValidator.validaCartaoIsento(cartao))
            throw new PagamentoException("Cartão inválido");
        Pagamento pagamento = new PagamentoIsento(valor, cartao);
        return gerarTicketCartao(pagamento, valor);
    }

    public Ticket pagarTicketComRecarregavel(float valor, String cartao) throws PagamentoException {
        if(!PagamentoValidator.validaCartaoRecarregavel(cartao))
            throw new PagamentoException("Cartão inválido");
        Pagamento pagamento = new PagamentoRecarregavel(valor, cartao);
        return gerarTicketCartao(pagamento, valor);
    }

    private Ticket gerarTicketMoeda(Pagamento pagamento, List<Moeda> moedasInseridas) {
        int tempo = ParquimetroUtil.getMinutosPorValor(parquimetro, moedasInseridas);
        tempo = (tempo > parquimetro.getTempoMaximo() ? parquimetro.getTempoMaximo() : tempo);
        return gerarTicket(pagamento, tempo);
    }

    private Ticket gerarTicketCartao(Pagamento pagamento, float valor) {
        int tempo = ParquimetroUtil.getMinutosPorValor(parquimetro, valor);
        return gerarTicket(pagamento, tempo);
    }

    private Ticket gerarTicket(Pagamento pagamento, int tempo) {
        Date emissao = new Date();
        Ticket ticket = new Ticket(ticketSerial, emissao, ParquimetroUtil.getValidade(emissao, tempo), parquimetro, pagamento);
        ticketSerial++;
        return ticket;
    }
}
