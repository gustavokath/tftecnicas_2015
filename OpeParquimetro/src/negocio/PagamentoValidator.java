/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package negocio;

import park.domain.Parquimetro;

/**
 *
 * @author Leonardo
 */
public class PagamentoValidator {

    public static boolean validaCartaoRecarregavel(String cartaoRecarregavel) {
        return cartaoRecarregavel.length() == 128;
    }

    public static boolean validaCartaoIsento(String cartaoIsento) {
        return cartaoIsento.length() == 128;
    }    
}
