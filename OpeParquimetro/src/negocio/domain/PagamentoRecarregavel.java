/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package negocio.domain;

import park.domain.Pagamento;

/**
 *
 * @author Leonardo
 */
public class PagamentoRecarregavel extends Pagamento {

    private String cartao;

    public PagamentoRecarregavel(float valor, String cartao) {
        super(valor);
        this.cartao = cartao;
    }

    public String getCartao() {
        return cartao;
    }

    public void setCartao(String cartao) {
        this.cartao = cartao;
    }
}
