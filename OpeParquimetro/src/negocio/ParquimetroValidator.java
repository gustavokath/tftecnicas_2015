/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package negocio;

import java.time.Instant;
import negocio.util.ParquimetroUtil;
import park.domain.Parquimetro;

/**
 *
 * @author 14104947
 */
public class ParquimetroValidator {

    public static boolean validaIdentificador(String identificador) {
        return identificador.length() == 5;
    }

    public static boolean validaValor(Parquimetro parquimetro, float valor) {
        if (valor < ParquimetroUtil.getValorMinimo(parquimetro)){
            return false;
        }
        return true;
    }

    public static boolean validaValorCartao(Parquimetro parquimetro, float valor) {
        if (valor < ParquimetroUtil.getValorMinimo(parquimetro) || valor > ParquimetroUtil.getValorMaximo(parquimetro)) {
            return false;
        }
        if (valor % parquimetro.getValorIncremento() != 0) {
            return false;
        }
        return true;
    }
}
