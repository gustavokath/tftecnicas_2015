CREATE TABLE "USUARIO"."MOEDAS"
(
  id_moeda INT PRIMARY KEY  GENERATED ALWAYS AS IDENTITY
        (START WITH 1, INCREMENT BY 1),
  id_parquimetro NUMERIC(11) NOT NULL,
  valor DECIMAL(5) NOT NULL
)
;

CREATE TABLE "USUARIO"."MOEDAS_PARQUIMETROS"(
  id_moeda INT,
  id_parquimetro INT  
)
;


CREATE TABLE "USUARIO"."PARQUIMETROS" (
  id_parquimetro INT PRIMARY KEY  GENERATED ALWAYS AS IDENTITY
        (START WITH 1, INCREMENT BY 1),
  endereco VARCHAR(100) NOT NULL,
  horario_ini_tarifacao VARCHAR(50) NOT NULL,
  horario_fim_tarifacao VARCHAR(50) NOT NULL,
  tempo_minimo NUMERIC(11) NOT NULL,
  tempo_maximo NUMERIC(11) NOT NULL,
  incremento NUMERIC(11) NOT NULL,
  valor_incremento DECIMAL(5) NOT NULL
)
;

CREATE TABLE "USUARIO"."TICKETS"(
  id_ticket INT PRIMARY KEY  GENERATED ALWAYS AS IDENTITY
        (START WITH 1, INCREMENT BY 1),
  data_hora_emissao DATE NOT NULL,
  data_hora_validade DATE NOT NULL,
  id_parquimetro INT NOT NULL,
  id_pagamento INT NOT NULL
)
;

CREATE TABLE "USUARIO"."PARQUIMETRO_MOEDAS" (
  id_parquimetro_moedas INT PRIMARY KEY  GENERATED ALWAYS AS IDENTITY
        (START WITH 1, INCREMENT BY 1),
  id_moeda NUMERIC (11) NOT NULL,
  id_parquimetro INT NOT NULL,
  id_pagamento INT
) 
;

CREATE TABLE "USUARIO"."PAGAMENTO"(
  id_pagamento INT PRIMARY KEY  GENERATED ALWAYS AS IDENTITY
        (START WITH 1, INCREMENT BY 1),
 valor DECIMAL(5) NOT NULL,
 data_pgto DATE NOT NULL
)
;

CREATE TABLE "USUARIO"."DINHEIRO"(
  id_pagamento INT
)
;

CREATE TABLE "USUARIO"."CARTAO_RESIDENTE"(
  id_pagamento INT,
 num_cartao VARCHAR(128) NOT NULL
)
;

CREATE TABLE "USUARIO"."CARTAO"(
  id_pagamento INT, 
 num_cartao VARCHAR(128)
)
;

