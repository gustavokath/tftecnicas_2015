/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package park.domain;

/**
 *
 * @author gustavokath
 */
public enum Moeda {
    UM_CENTAVO((float) 0.01),
    CINCO_CENTAVOS((float) 0.05),
    DEZ_CENTAVOS((float) 0.10),
    VINTE_E_CINCO_CENTAVOS((float) 0.25),
    CINQUENTA_CENTAVOS((float) 0.5),
    UM_REAL((float) 1);

    private float numVal;

    Moeda(float numVal) {
        this.numVal = numVal;
    }

    public float getNumVal() {
        return numVal;
    }
}
