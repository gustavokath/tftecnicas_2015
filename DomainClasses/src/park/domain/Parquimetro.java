package park.domain;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author gustavokath
 */
public class Parquimetro {
    private int identificacao;
    private String endereco;
    private String horarioIniTarifacao; //trocar pra um time
    private String horarioFimTarifacao; //trocar pra um time
    private int tempoMinimo;
    private int tempoMaximo;
    private float valorIncremento;
    private int incremento;
    private List<Moeda> moedasPermitidas;

    public Parquimetro(int identificacao, String endereco, String horarioIniTarifacao, String horarioFimTarifacao, int tempoMinimo, int tempoMaximo, float valorIncremento, int incremento) {
        this.identificacao = identificacao;
        this.endereco = endereco;
        this.horarioIniTarifacao = horarioIniTarifacao;
        this.horarioFimTarifacao = horarioFimTarifacao;
        this.tempoMinimo = tempoMinimo;
        this.tempoMaximo = tempoMaximo;
        this.valorIncremento = valorIncremento;
        this.incremento = incremento;
        this.moedasPermitidas = new ArrayList<Moeda>();
    }

    public int getIdentificacao() {
        return identificacao;
    }

    public void setIdentificacao(int identificacao) {
        this.identificacao = identificacao;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public String getHorarioIniTarifacao() {
        return horarioIniTarifacao;
    }

    public void setHorarioIniTarifacao(String horarioIniTarifacao) {
        this.horarioIniTarifacao = horarioIniTarifacao;
    }

    public String getHorarioFimTarifacao() {
        return horarioFimTarifacao;
    }

    public void setHorarioFimTarifacao(String horarioFimTarifacao) {
        this.horarioFimTarifacao = horarioFimTarifacao;
    }

    public int getTempoMinimo() {
        return tempoMinimo;
    }

    public void setTempoMinimo(int tempoMinimo) {
        this.tempoMinimo = tempoMinimo;
    }

    public int getTempoMaximo() {
        return tempoMaximo;
    }

    public void setTempoMaximo(int tempoMaximo) {
        this.tempoMaximo = tempoMaximo;
    }

    public float getValorIncremento() {
        return valorIncremento;
    }

    public void setValorIncremento(float valorIncremento) {
        this.valorIncremento = valorIncremento;
    }

    public int getIncremento() {
        return incremento;
    }

    public void setIncremento(int incremento) {
        this.incremento = incremento;
    }

    public List<Moeda> getMoedasPermitidas() {
        return moedasPermitidas;
    }

    public void setMoedasPermitidas(List<Moeda> moedasPermitidas) {
        this.moedasPermitidas = moedasPermitidas;
    }    
}
