/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package park.domain;

import java.time.Instant;
import java.util.Date;

/**
 *
 * @author gustavokath
 */
public abstract class Pagamento {
    private float valor;
    private Date dataHora;

    public Pagamento(float valor) {
        this.valor = valor;
        dataHora = Date.from(Instant.now());
    }
    
    public Pagamento(float valor, Date dataHora) {
        this.valor = valor;
        dataHora = Date.from(Instant.now());
    }
    
    public Date getDataHora(){
        return dataHora;
    }
    public void setDataHora(Date dataHora){
        this.dataHora = dataHora;
    }
    
    public  float getValor(){
        return valor;
    }
        
    public  void setValor(float valor){
        valor = this.valor;
    }
}
