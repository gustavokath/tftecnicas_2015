/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package park.domain;

import java.util.Date;

/**
 *
 * @author gustavokath
 */
public class Ticket {
    private int serial;
    private Date dataHoraEmissao;
    private Date dataHoraValidade;
    private Parquimetro parquimetro;
    private Pagamento pagamento;

    public Ticket(int serial, Date dataHoraEmissao, Date dataHoraValidade, Parquimetro parquimetro, Pagamento pagamento) {
        this.serial = serial;
        this.dataHoraEmissao = dataHoraEmissao;
        this.dataHoraValidade = dataHoraValidade;
        this.parquimetro = parquimetro;
        this.pagamento = pagamento;
    }
    
    public Ticket(Date dataHoraEmissao, Date dataHoraValidade, Parquimetro parquimetro, Pagamento pagamento) {
        this.dataHoraEmissao = dataHoraEmissao;
        this.dataHoraValidade = dataHoraValidade;
        this.parquimetro = parquimetro;
        this.pagamento = pagamento;
    }
    

    public int getSerial() {
        return serial;
    }

    public void setSerial(int serial) {
        this.serial = serial;
    }

    public Date getDataHoraEmissao() {
        return dataHoraEmissao;
    }

    public Date getDataHoraValidade() {
        return dataHoraValidade;
    }

    public void setDataHoraValidade(Date dataHoraValidade) {
        this.dataHoraValidade = dataHoraValidade;
    }

    public Parquimetro getParquimetro() {
        return parquimetro;
    }

    public Pagamento getPagamento() {
        return pagamento;
    }
}
