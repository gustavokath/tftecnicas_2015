/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package negocio.domain;
import java.time.Instant;
import java.util.Date;
import park.domain.Pagamento;
/**
 *
 * @author gustavokath
 */
public class Dinheiro extends Pagamento{

    public Dinheiro(float valor) {
        super(valor);
    }
    public Dinheiro(float valor, Date dataHora) {
        super(valor, dataHora);
    }
}
