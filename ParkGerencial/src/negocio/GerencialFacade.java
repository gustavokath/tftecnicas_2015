/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package negocio;
import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import negocio.dao.*;
import park.domain.Pagamento;
import park.domain.Parquimetro;
import park.domain.Ticket;
import persistencia.PagamentoDAODerby;
import persistencia.ParquimetroDAODerby;
import persistencia.TicketDAODerby;

/**
 *
 * @author gustavokath
 */
public class GerencialFacade {
    private PagamentoDAO pagamentoDAO;
    private ParquimetroDAO parquimetroDAO;
    private TicketDAO ticketDAO;

    public GerencialFacade() {
        this.pagamentoDAO = new PagamentoDAODerby();
        this.parquimetroDAO = new ParquimetroDAODerby();
        this.ticketDAO = new TicketDAODerby();
    }

    public List<String> getAnosRegistros() {
        List<String> l = new ArrayList<>();
        l.add("Teste");
        return l;
    }
    
    public List<Parquimetro> getParquimetros(){
        List<Parquimetro> lista = null;
        try {
            lista = parquimetroDAO.buscarTodos();
        } catch (ParquimetroDAOException ex) {
            Logger.getLogger(GerencialFacade.class.getName()).log(Level.SEVERE, null, ex);
        }
        return lista;
    }
    
    public List<Ticket> getTickets(int idParquimetro){
        List<Ticket> lista = null;
        try {
            lista = ticketDAO.buscaTicketsParquimetro(idParquimetro);
        } catch (TicketDAOException ex) {
            Logger.getLogger(GerencialFacade.class.getName()).log(Level.SEVERE, null, ex);
        }
        return lista;
    }
    
     public List<Ticket> getTickets(int dia, int mes){
        List<Ticket> lista = null;
        try {
            lista = ticketDAO.buscarTodos(dia, mes);
        } catch (TicketDAOException ex) {
            Logger.getLogger(GerencialFacade.class.getName()).log(Level.SEVERE, null, ex);
        }
        return lista;
    }
    
    public List<Pagamento> getPagamentos(){
        List<Pagamento> lista = null;
        try {
            lista = pagamentoDAO.buscarTodos();
        } catch (PagamentoDAOException ex) {
            Logger.getLogger(GerencialFacade.class.getName()).log(Level.SEVERE, null, ex);
        }
        return lista;
    }
    
    public Pagamento getPagamento(int codigo){
        Pagamento p = null;
        try {
            p = pagamentoDAO.buscarPorCodigo(codigo);
        } catch (PagamentoDAOException ex) {
            Logger.getLogger(GerencialFacade.class.getName()).log(Level.SEVERE, null, ex);
        }
       return p; 
    }
    
    public void getArrecadacaoPagos(String idParquimetro, String agroup, List<Float> valores, List<String> agroupList){
        try {
            pagamentoDAO.buscaPagos(idParquimetro, agroup, valores, agroupList);
        } catch (PagamentoDAOException ex) {
            Logger.getLogger(GerencialFacade.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void getArrecadacaoIsentos(String idParquimetro, String agroup, List<Float> valores, List<String> agroupList){
        try {
            pagamentoDAO.buscaIsentos(idParquimetro, agroup, valores, agroupList);
        } catch (PagamentoDAOException ex) {
            Logger.getLogger(GerencialFacade.class.getName()).log(Level.SEVERE, null, ex);
        }
    }  
    
    public Float getTotalArrecadadoPeriodo(Date dataIni, Date dataFim){
        Float value = 0.0f;
        try {
            value = pagamentoDAO.buscaTotalArrecadado(dataIni, dataFim);
        } catch (PagamentoDAOException ex) {
            Logger.getLogger(GerencialFacade.class.getName()).log(Level.SEVERE, null, ex);
        }
        return value;
    }
    
    public Float getTotalIsentoPeriodo(Date dataIni, Date dataFim){
        Float value = 0.0f;
        try {
            value = pagamentoDAO.buscaTotalIsento(dataIni, dataFim);
        } catch (PagamentoDAOException ex) {
            Logger.getLogger(GerencialFacade.class.getName()).log(Level.SEVERE, null, ex);
        }
        return value;
    }
    
    public boolean importFile(String filePath){
        LeituraJSon leituraJSon = new LeituraJSon(this);
        boolean result = false;
        try{
            File file = new File(filePath);
            result = leituraJSon.importData();
        }catch(Exception e){
            return false;
        }
        return result;
    }
}
