/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package negocio.dao;

/**
 *
 * @author Weirich
 */

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import javax.sql.DataSource;
import org.apache.derby.jdbc.ClientDataSource;

public class InitDB {

    public static String DB_CONN_STRING = "jdbc:derby://localhost:1527/Tecnicas";
    public static String DB_NAME = "TrabalhoFinal";
    public static String USER_NAME = "usuario";
    public static String PASSWORD = "senha";
    private static DataSource dataSource;

    private static Connection getConexaoViaDriverManager() throws Exception {
        return DriverManager.getConnection(DB_CONN_STRING, USER_NAME, PASSWORD);
    }
    
    private static DataSource criarDataSource() throws Exception {
        if (dataSource == null) {
            ClientDataSource ds = new ClientDataSource();
            ds.setDatabaseName(DB_NAME);
            ds.setCreateDatabase("create");
            ds.setUser(USER_NAME);
            ds.setPassword(PASSWORD);
            dataSource = ds;
        }
        return dataSource;
    }

//    public static void criarBd() throws Exception {
//        try (Connection con = criarDataSource().getConnection();
//                Statement sta = con.createStatement()) {
//            String sql = "CREATE TABLE Contribuintes ("
//                    + "NOME VARCHAR(100) NOT NULL,"
//                    + "CPF VARCHAR(20) PRIMARY KEY,"
//                    + "IDADE NUMERIC(3) NOT NULL,"
//                    + "NRODEP NUMERIC(2) NOT NULL,"
//                    + "CONTRPREV DECIMAL(10,2) NOT NULL,"
//                    + "TOTREND DECIMAL(10,2) NOT NULL"
//                    + ")";
//            sta.executeUpdate(sql);
//        }
//    }

    public static Connection conectarBd() throws Exception {
        return criarDataSource().getConnection();
    }
}

