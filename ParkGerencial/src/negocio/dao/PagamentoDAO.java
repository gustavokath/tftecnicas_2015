/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package negocio.dao;

import java.util.Date;
import java.util.List;
import park.domain.Pagamento;

/**
 *
 * @author gustavokath
 */
public interface PagamentoDAO {
    List<Pagamento> buscarTodos() throws PagamentoDAOException;
    Float buscaTotalIsento(Date inicio, Date fim) throws PagamentoDAOException;
    Float buscaTotalArrecadado(Date inicio, Date fim) throws PagamentoDAOException;
    void buscaIsentos(String idParquimetro, String agrup, List<Float> valores, List<String> datas) throws PagamentoDAOException;
    void buscaPagos(String idParquimetro, String agrup, List<Float> valores, List<String> datas) throws PagamentoDAOException;
    Pagamento buscarPorCodigo(int codigo) throws PagamentoDAOException;
}
