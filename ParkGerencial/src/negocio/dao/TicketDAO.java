/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package negocio.dao;

import java.util.List;
import park.domain.Parquimetro;
import java.time.Instant;
import park.domain.Ticket;

/**
 *
 * @author gustavokath
 */
public interface TicketDAO {
    List<Ticket> buscarTodos() throws TicketDAOException;
    List<Ticket> buscarTodos(int dia, int mes) throws TicketDAOException;
    List<Ticket> buscaTicketsParquimetro(int idParquimetro) throws TicketDAOException;
    List<Ticket> buscaTicketsParquimetro(int idParquimetro, int ano) throws TicketDAOException;
}
