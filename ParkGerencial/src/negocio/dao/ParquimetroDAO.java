/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package negocio.dao;

import java.util.List;
import park.domain.Parquimetro;

/**
 *
 * @author gustavokath
 */
public interface ParquimetroDAO {
    List<Parquimetro> buscarTodos() throws ParquimetroDAOException;
    Parquimetro buscarPorCodigo(int codigo) throws ParquimetroDAOException;
    float totalArrecadado(int codigo) throws ParquimetroDAOException;
    List<Float> totalArrecadadoAno(int codigo) throws ParquimetroDAOException;
    void inserir(Parquimetro parquimetro) throws ParquimetroDAOException;
    void alterar(Parquimetro parquimetro) throws ParquimetroDAOException;  
}
