/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import negocio.GerencialFacade;
import park.domain.*;
/**
 *
 * @author gustavokath
 */
public class MainWindowController {
    
    private MainWindowView mainWindow;
    private RelatorioController relatorioControler;
    private GerencialFacade facade;
    
    public MainWindowController(MainWindowView mainWindowView){
        mainWindow = mainWindowView;
        facade = new GerencialFacade();
    }
    
    private void abrirRelatorio(RelatorioController controller){
        relatorioControler = controller;
        mainWindow.setContentPane(relatorioControler.getRelatorioView());
        mainWindow.pack();
    }
    
    public void abrirRelatorioPizza(){
        abrirRelatorio(new GraficoPizzaController(facade));
    }
    
    public void abreRelatorioGeral(){
        abrirRelatorio(new RelatorioGeralController(facade));
    }
    
    public void abreRelatorioArrecadacao(){
        abrirRelatorio(new RelatorioArrecadacaoControler(facade));
    }
    
    public void abreGraficoBarras(){
        abrirRelatorio(new GraficoBarrasController(facade));
    }
    
    public void abreImportView(){
        abrirRelatorio(new ImportFileController(facade));
    }
    
}
