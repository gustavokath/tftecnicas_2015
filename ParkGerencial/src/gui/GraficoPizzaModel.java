/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import java.util.Date;
import org.jfree.data.general.DatasetGroup;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.data.general.PieDataset;

/**
 *
 * @author gustavokath
 */
public class GraficoPizzaModel {
    private GraficoPizzaController controller;

    public GraficoPizzaModel(GraficoPizzaController controller) {
        this.controller = controller;
    }
    
    public void requestRelatorioData(Date dateIni, Date dateFim){
        DefaultPieDataset dataset = new DefaultPieDataset();
        float valorArrecadado = controller.getFacade().getTotalArrecadadoPeriodo(dateIni, dateFim);
        float valorIsento = controller.getFacade().getTotalIsentoPeriodo(dateIni, dateFim);
        dataset.setValue("Arrecadado", valorArrecadado);
        dataset.setValue("Isento", valorIsento);
        uploadRelatorio(dataset);
    }
    
    public void uploadRelatorio(PieDataset data){
        controller.uploadRelatorio(data);
    }
}
