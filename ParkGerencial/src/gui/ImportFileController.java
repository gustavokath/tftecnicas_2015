/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import java.io.File;
import javax.swing.JPanel;
import negocio.GerencialFacade;
import negocio.LeituraJSon;

/**
 *
 * @author 14104915
 */
public class ImportFileController implements RelatorioController{
    private GerencialFacade facade;
    private ImportFileModel model;
    private ImportFileView view;
    private LeituraJSon leituraJSon;
    
    public ImportFileController(GerencialFacade facade) {
        this.facade = facade;
        initialize();
    }

    @Override
    public void initialize() {
        model = new ImportFileModel(this);
        view = new ImportFileView(this);
        
    }

    @Override
    public JPanel getRelatorioView() {
        return view;
    }

    @Override
    public GerencialFacade getFacade() {
        return facade;
    }
    
    public boolean importFile(String filePath){
        return facade.importFile(filePath);
    }
    
}
