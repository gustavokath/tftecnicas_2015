/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import java.util.List;
import javax.swing.JPanel;
import negocio.GerencialFacade;
import org.jfree.data.category.CategoryDataset;

/**
 *
 * @author 14104915
 */
public class GraficoBarrasController implements RelatorioController{
    GraficoBarrasModel model;
    GraficoBarrasView view;
    GerencialFacade facade;

    public GraficoBarrasController(GerencialFacade facade) {
        this.facade = facade;
        initialize();
    }

    @Override
    public void initialize() {
        model = new GraficoBarrasModel(this);
        view = new GraficoBarrasView(this);
        model.requestParquimetos();
        view.criaGrafico();
    }

    @Override
    public JPanel getRelatorioView() {
        return view;
    }

    @Override
    public GerencialFacade getFacade() {
        return facade;
    }
    
    public CategoryDataset requestRelatorio(){
        return model.requestRelatorioData();
    }
}
