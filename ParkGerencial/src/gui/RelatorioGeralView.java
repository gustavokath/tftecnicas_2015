/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.SwingWorker;


/**
 *
 * @author gustavokath
 */
public class RelatorioGeralView extends javax.swing.JPanel {
    
    private final RelatorioGeralController controller;
    
    /**
     * Creates new form RelatorioGeralView
     */
    public RelatorioGeralView(RelatorioGeralController controller) {
        this.controller = controller;
        initComponents();
        
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jToolBar2 = new javax.swing.JToolBar();
        jLabel1 = new javax.swing.JLabel();
        mesesSelector = new javax.swing.JComboBox();
        jSeparator1 = new javax.swing.JToolBar.Separator();
        jLabel2 = new javax.swing.JLabel();
        diaSelector = new javax.swing.JComboBox();
        scrollPane1 = new java.awt.ScrollPane();
        jScrollPane1 = new javax.swing.JScrollPane();
        textoRelatorio = new javax.swing.JTextPane();

        setPreferredSize(new java.awt.Dimension(698, 486));

        jToolBar2.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jToolBar2.setFloatable(false);

        jLabel1.setText("Dia: ");
        jToolBar2.add(jLabel1);

        mesesSelector.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12" }));
        mesesSelector.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mesesSelectorActionPerformed(evt);
            }
        });
        jToolBar2.add(mesesSelector);
        jToolBar2.add(jSeparator1);

        jLabel2.setText("Mês: ");
        jToolBar2.add(jLabel2);

        diaSelector.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", " ", " " }));
        diaSelector.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                diaSelectorActionPerformed(evt);
            }
        });
        jToolBar2.add(diaSelector);

        jScrollPane1.setViewportView(textoRelatorio);

        scrollPane1.add(jScrollPane1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jToolBar2, javax.swing.GroupLayout.DEFAULT_SIZE, 698, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addComponent(scrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jToolBar2, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(scrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 441, Short.MAX_VALUE)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void mesesSelectorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mesesSelectorActionPerformed
       requestRelatorioGeral();
    }//GEN-LAST:event_mesesSelectorActionPerformed

    private void diaSelectorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_diaSelectorActionPerformed
        requestRelatorioGeral();
    }//GEN-LAST:event_diaSelectorActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox diaSelector;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JToolBar.Separator jSeparator1;
    private javax.swing.JToolBar jToolBar2;
    private javax.swing.JComboBox mesesSelector;
    private java.awt.ScrollPane scrollPane1;
    private javax.swing.JTextPane textoRelatorio;
    // End of variables declaration//GEN-END:variables

    private void initialize() {
        requestRelatorioGeral();
    }
    
    private void requestRelatorioGeral(){
        controller.requestRelatorioGeral(Integer.parseInt((String) diaSelector.getSelectedItem()), Integer.parseInt((String) mesesSelector.getSelectedItem()));
    }
    
    public void updateTextoRelatorio(String texto){
        textoRelatorio.setText(texto);
    }
}
