/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import java.util.ArrayList;
import java.util.List;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;
import park.domain.Parquimetro;

/**
 *
 * @author 14104915
 */
public class GraficoBarrasModel {
    private List<Parquimetro> listaParquimetros;
    private GraficoBarrasController controler;
    private List<Float> valuesList;
    private List<String> agrupList;
    
    
    public GraficoBarrasModel(GraficoBarrasController controler) {
        this.controler = controler;
    }
    
    public List<Parquimetro> getParquimetros(){
        return listaParquimetros;
    }
    
    public void requestParquimetos(){
        listaParquimetros = controler.getFacade().getParquimetros();
    }
    
    public CategoryDataset requestRelatorioData(){
        DefaultCategoryDataset dataset = new DefaultCategoryDataset();
        agrupList = new ArrayList<>();
        valuesList = new ArrayList<>();
        for(Parquimetro p : listaParquimetros){
            controler.getFacade().getArrecadacaoPagos(Integer.toString(p.getIdentificacao()), "ano", valuesList, agrupList);
            for(int i=0;i<valuesList.size();i++)
                dataset.addValue(valuesList.get(i), "Parq:"+p.getIdentificacao()+"/"+agrupList.get(i), "Ano");
        }
        return dataset;
    }
}
