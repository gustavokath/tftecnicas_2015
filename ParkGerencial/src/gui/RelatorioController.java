/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import javax.swing.JPanel;
import negocio.GerencialFacade;

/**
 *
 * @author gustavokath
 */
public interface RelatorioController {
    void initialize();
    JPanel getRelatorioView();
    GerencialFacade getFacade();
}
