/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import java.util.Date;
import javax.swing.JPanel;
import negocio.GerencialFacade;
import org.jfree.data.general.PieDataset;

/**
 *
 * @author gustavokath
 */
public class GraficoPizzaController implements RelatorioController{
    private GerencialFacade facade;
    private GraficoPizzaModel model;
    private GraficoPizzaView view;

    public GraficoPizzaController(GerencialFacade facade) {
        this.facade = facade;
        initialize();
    }

    @Override
    public void initialize() {
        model = new GraficoPizzaModel(this);
        view = new GraficoPizzaView(this);
    }

    @Override
    public JPanel getRelatorioView() {
        return view;
    }

    @Override
    public GerencialFacade getFacade() {
        return facade;
    }
    
    public void requestRelatorioData(Date dateIni, Date dateFim){
        model.requestRelatorioData(dateIni, dateFim);
    }
    
    public void uploadRelatorio(PieDataset data){
        view.upadateRelatorio(data);
    }
}
