/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import java.util.ArrayList;
import java.util.List;
import javax.swing.JPanel;
import negocio.GerencialFacade;
import negocio.Meses;

/**
 *
 * @author gustavokath
 */
public class RelatorioGeralController implements RelatorioController{
    
    private RelatorioGeralModel relatorioGeralModel;
    private RelatorioGeralView relatorioGeralView;
    private GerencialFacade facade;
    
    public RelatorioGeralController(GerencialFacade facade){
        this.facade = facade;
        initialize();
    }
    
     @Override
    public void initialize() {
        relatorioGeralModel = new RelatorioGeralModel(this);
        relatorioGeralView = new RelatorioGeralView(this);
    }
    
    @Override
    public JPanel getRelatorioView() {
        return relatorioGeralView;
    }
    
    public String[] getMesesTextBoxModel(){
        return Meses.nomeMeses;
    }

    Object[] loadAnoSelector() {
        return relatorioGeralModel.getAnosRegistros();
    }

    @Override
    public GerencialFacade getFacade() {
        return facade;
    }
    
    public void requestRelatorioGeral(int dia, int mes){
        relatorioGeralModel.getRelatorioGeral(dia,mes);
    }
    
    public void updateRelatorioGeral(String text){
        if(text.equals("\n,"))
            text = "Nenhum valor encontrado";
        relatorioGeralView.updateTextoRelatorio(text);
    }
}
