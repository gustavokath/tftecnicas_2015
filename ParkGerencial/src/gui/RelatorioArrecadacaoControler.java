/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import javax.swing.JPanel;
import negocio.GerencialFacade;

/**
 *
 * @author 14104915
 */
public class RelatorioArrecadacaoControler implements RelatorioController{
    RelatorioArrecadacaoModel model;
    RelatorioArrecadacaoView view;
    GerencialFacade facade;

    public RelatorioArrecadacaoControler(GerencialFacade facade) {
        this.facade = facade;
        this.initialize();
    }

    @Override
    public void initialize() {
        model = new RelatorioArrecadacaoModel(this);
        view = new RelatorioArrecadacaoView(this);
        model.requestParuimetrosIds();
    }

    @Override
    public JPanel getRelatorioView() {
        return view;
    }

    @Override
    public GerencialFacade getFacade() {
        return facade;
    }
    
    public void requestParquimetrosIds(){
        model.requestParuimetrosIds();
    }
    
    public void uploadParquimetrosIds(Object[] idsList){
        view.uploadParquimetrosIds(idsList);
    }
    
    public void requestRelatorio(String idParquimetro, String agoup){
        model.requestRelatorioArrecadacao(idParquimetro, agoup);
    }
    
    public void uploadRelatorio(String texto){
        view.updateRelatorio(texto);
    }
    
}
