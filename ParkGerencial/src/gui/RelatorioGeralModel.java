/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import java.util.List;
import park.domain.*;
import negocio.GerencialFacade;

/**
 *
 * @author gustavokath
 */
public class RelatorioGeralModel {

    private List<Parquimetro> listaParquimetros;
    private List<Ticket> listaTickets;
    private List<Pagamento> listaPagamentos;
    private RelatorioGeralController controller;

    public RelatorioGeralModel(RelatorioGeralController controller) {
        this.controller = controller;
    }

    public Object[] getAnosRegistros() {
        List<String> anos = controller.getFacade().getAnosRegistros();
        return anos.toArray();
    }

    public void getRelatorioGeral(int dia, int mes) {
        StringBuilder strBuild = new StringBuilder();
        listaTickets = controller.getFacade().getTickets(dia, mes);
        for (Ticket t : listaTickets) {
            Parquimetro park = t.getParquimetro();
            strBuild.append("\nId: ").append(t.getSerial());
            strBuild.append("\nHora Emissao: ").append(t.getDataHoraEmissao().toString());
            strBuild.append("\nValidade: ").append(t.getDataHoraValidade().toString());
            strBuild.append("\nPagamento: [ ");
            strBuild.append("\n\tValor: ").append(t.getPagamento().getValor());
            strBuild.append("\n]");
            strBuild.append("Parquimetro:[ ");
            strBuild.append("\n\tId: ").append(park.getIdentificacao());
            strBuild.append("\n\tEndereço: ").append(park.getEndereco());
            strBuild.append("\n\tHora Inicio: ").append(park.getHorarioIniTarifacao());
            strBuild.append("\n\tHora Fim: ").append(park.getHorarioFimTarifacao());
            strBuild.append("\n]");
        }
        strBuild.append("\n,");
        controller.updateRelatorioGeral(strBuild.toString());
    }
}
