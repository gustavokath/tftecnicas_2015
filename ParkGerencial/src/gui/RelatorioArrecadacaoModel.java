/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import java.util.ArrayList;
import java.util.List;
import park.domain.Parquimetro;

/**
 *
 * @author 14104915
 */
public class RelatorioArrecadacaoModel {
    private List<Parquimetro> listaParquimetros;
    private RelatorioArrecadacaoControler controler;

    public RelatorioArrecadacaoModel(RelatorioArrecadacaoControler controler) {
        this.controler = controler;
    }
    
    public List<Parquimetro> getParquimetros(){
        return listaParquimetros;
    }
    
    public void requestParquimetos(){
        listaParquimetros = controler.getFacade().getParquimetros();
    }
    
    public Object[] getParquimetrosId(){
        ArrayList<String> ids = new ArrayList<>();
        for(Parquimetro p : listaParquimetros)
            ids.add(Integer.toString(p.getIdentificacao()));
        return ids.toArray();
    }
    
    public void requestParuimetrosIds(){
        requestParquimetos();
        Object[] idsList = getParquimetrosId();
        controler.uploadParquimetrosIds(idsList);
    }
    
    public void requestRelatorioArrecadacao(String idParquimetro, String agrupamento){
        List<Float> valores = new ArrayList<>();
        List<String> agroupList = new ArrayList<>();
        StringBuilder strBuild = new StringBuilder();
        controler.getFacade().getArrecadacaoPagos(idParquimetro, agrupamento, valores, agroupList);
        strBuild.append("Parquimetro ").append(idParquimetro);
        strBuild.append("\nValor Arrecadado: [");
        for(int i=0;i<valores.size();i++){
            strBuild.append("\n\t").append(agroupList.get(i)).append(": R$").append(valores.get(i));
        }
        strBuild.append("]\n");
        valores = new ArrayList<>();
        agroupList = new ArrayList<>();
        controler.getFacade().getArrecadacaoIsentos(idParquimetro, agrupamento, valores, agroupList);
        strBuild.append("\nValor Isentos: [");
        for(int i=0;i<valores.size();i++){
            strBuild.append("\n\t").append(agroupList.get(i)).append(": R$").append(valores.get(i));
        }
        strBuild.append("]\n");
        controler.uploadRelatorio(strBuild.toString());
    }
    
}
