/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistencia;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import negocio.dao.InitDB;
import negocio.dao.PagamentoDAOException;
import negocio.dao.TicketDAO;
import negocio.dao.TicketDAOException;
import park.domain.*;
import persistencia.PagamentoDAODerby;

/**
 *
 * @author Weirich
 */
public class TicketDAODerby implements TicketDAO {
    public int selectUltimoPagamento() throws PagamentoDAOException{
          String sql = "select max(id_pagamento) as id from pagamento";
          int val = 1;  
          try (Connection conexao = InitDB.conectarBd()) {
                try (PreparedStatement comando = conexao.prepareStatement(sql)) {
                    try (ResultSet resultado = comando.executeQuery()) {
                        
                        while (resultado.next()) {
                            val = resultado.getInt("id");
                        }
                    }
                }
            } catch (Exception e) {
                throw new PagamentoDAOException();
            }
            return val;
    }
    
    public void inserirTicket(Date a, Date b, int id_parquimetro) {
     String sql = "insert into tickets(data_hora_emissao,data_hora_validade, id_parquimetro, id_pagamento) values(?,?,?,?);";
     try (Connection conexao = InitDB.conectarBd()) {
       try (PreparedStatement comando = conexao.prepareStatement(sql)) {
           comando.setDate(1, (java.sql.Date) a);
           comando.setDate(2, (java.sql.Date) b);
           comando.setInt(3, id_parquimetro);
           comando.setInt(4, selectUltimoPagamento());
                if (comando.executeUpdate() > 0) {
                    System.out.println("Inserção efetuada com sucesso");
                } else {
                    System.out.println("Falha na inserção");
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    
    }

    @Override
    public List<Ticket> buscarTodos() throws TicketDAOException {
        List<Ticket> listaTickets = new ArrayList<>();
        String sql = "select * from tickets";
        try (Connection conexao = InitDB.conectarBd()) {
            try (PreparedStatement comando = conexao.prepareStatement(sql)) {
                try (ResultSet resultado = comando.executeQuery()) {
                    while (resultado.next()) {
                        int id_ticket = resultado.getInt("id_ticket");
                        Date data_hora_emissao = resultado.getDate("data_hora_emissao");
                        Date data_hora_validade = resultado.getDate("data_hora_validade");
                        int id_parquimetro = resultado.getInt("id_parquimetro");
                        int id_pagamento = resultado.getInt("id_pagamento");

                        ParquimetroDAODerby parqDerby = new ParquimetroDAODerby();
                        Parquimetro parquimetro = parqDerby.buscarPorCodigo(id_parquimetro);

                        PagamentoDAODerby pgtoDerby = new PagamentoDAODerby();
                        Pagamento pagamento = pgtoDerby.buscarPorCodigo(id_pagamento);

                        listaTickets.add(new Ticket(id_ticket, data_hora_emissao, data_hora_validade, parquimetro, pagamento));
                    }
                    return listaTickets;
                }
            }
        } catch (Exception e) {
            throw new TicketDAOException();
        }
    }

    public List<Ticket> buscarTodos(int dia, int mes) throws TicketDAOException {
        String comandoWhere;
        if (dia == 0) {
            comandoWhere = " month(data_hora_emissao) = '"+ mes +"'";   
        } else if (mes == 0) {
            comandoWhere = " day(data_hora_emissao)  = '" + dia +"'";
        }
        else{
            comandoWhere = " month(data_hora_emissao) = "+ mes+" and" + " day(data_hora_emissao)  = " + dia +"";
        }
        List<Ticket> listaTickets = new ArrayList<>();
        String sql = "select * from tickets where" + comandoWhere;
        try (Connection conexao = InitDB.conectarBd()) {
            try (PreparedStatement comando = conexao.prepareStatement(sql)) {
                try (ResultSet resultado = comando.executeQuery()) {
                    while (resultado.next()) {
                        int id_ticket = resultado.getInt("id_ticket");
                        Date data_hora_emissao = resultado.getDate("data_hora_emissao");
                        Date data_hora_validade = resultado.getDate("data_hora_validade");
                        int id_parquimetro = resultado.getInt("id_parquimetro");
                        int id_pagamento = resultado.getInt("id_pagamento");

                        PagamentoDAODerby pgtoDerby = new PagamentoDAODerby();
                        Pagamento pagamento = pgtoDerby.buscarPorCodigo(id_pagamento);
                        
                        ParquimetroDAODerby parqDerby = new ParquimetroDAODerby();
                        Parquimetro parquimetro = parqDerby.buscarPorCodigo(id_parquimetro);

                        

                        listaTickets.add(new Ticket(id_ticket, data_hora_emissao, data_hora_validade, parquimetro, pagamento));
                    }
                    return listaTickets;
                }
            }
        } catch (Exception e) {
            throw new TicketDAOException();
        }
    }

    @Override
    public List<Ticket> buscaTicketsParquimetro(int  idParquimetro) throws TicketDAOException {
         List<Ticket> listaTickets = new ArrayList<>();
        String sql = "select * from tickets where id_parquimetro = "+idParquimetro;
        try (Connection conexao = InitDB.conectarBd()) {
            try (PreparedStatement comando = conexao.prepareStatement(sql)) {
                try (ResultSet resultado = comando.executeQuery()) {
                    while (resultado.next()) {
                        int id_ticket = resultado.getInt("id_ticket");
                        Date data_hora_emissao = resultado.getDate("data_hora_emissao");
                        Date data_hora_validade = resultado.getDate("data_hora_validade");
                        int id_parquimetro = resultado.getInt("id_parquimetro");
                        int id_pagamento = resultado.getInt("id_pagamento");

                        ParquimetroDAODerby parqDerby = new ParquimetroDAODerby();
                        Parquimetro parquimetro = parqDerby.buscarPorCodigo(id_parquimetro);

                        PagamentoDAODerby pgtoDerby = new PagamentoDAODerby();
                        Pagamento pagamento = pgtoDerby.buscarPorCodigo(id_pagamento);

                        listaTickets.add(new Ticket(id_ticket, data_hora_emissao, data_hora_validade, parquimetro, pagamento));
                    }
                    return listaTickets;
                }
            }
        } catch (Exception e) {
            throw new TicketDAOException();
        }
    }

    @Override
    public List<Ticket> buscaTicketsParquimetro(int idParquimetro, int ano) throws TicketDAOException {
         List<Ticket> listaTickets = new ArrayList<>();
        String sql = "select tickets.id_ticket, tickets.data_hora_emissao, tickets.data_hora_validade, tickets.id_parquimetro, pagamento.id_pagamento from tickets inner join "
                + "pagamento on tickets.id_pagamento = pagamento.id_pagamento "
                + "where tickets.year(data_hora_emissao) = " + ano+" and tickets.id_parquimetro = " + idParquimetro; 
        try (Connection conexao = InitDB.conectarBd()) {
            try (PreparedStatement comando = conexao.prepareStatement(sql)) {
                try (ResultSet resultado = comando.executeQuery()) {
                    while (resultado.next()) {
                        int id_ticket = resultado.getInt("id_ticket");
                        Date data_hora_emissao = resultado.getDate("data_hora_emissao");
                        Date data_hora_validade = resultado.getDate("data_hora_validade");
                        int id_parquimetro = resultado.getInt("id_parquimetro");
                        int id_pagamento = resultado.getInt("id_pagamento");

                        ParquimetroDAODerby parqDerby = new ParquimetroDAODerby();
                        Parquimetro parquimetro = parqDerby.buscarPorCodigo(id_parquimetro);

                        PagamentoDAODerby pgtoDerby = new PagamentoDAODerby();
                        Pagamento pagamento = pgtoDerby.buscarPorCodigo(id_pagamento);

                        listaTickets.add(new Ticket(id_ticket, data_hora_emissao, data_hora_validade, parquimetro, pagamento));
                    }
                    return listaTickets;
                }
            }
        } catch (Exception e) {
            throw new TicketDAOException();
        }
    }

}
