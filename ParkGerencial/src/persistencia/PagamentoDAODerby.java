/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistencia;

import java.sql.Connection;
import java.util.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import negocio.dao.InitDB;
import negocio.dao.PagamentoDAO;
import negocio.dao.PagamentoDAOException;
import negocio.dao.TicketDAOException;
import negocio.dao.TiposAgrupamento;
import park.domain.Pagamento;
import negocio.domain.Cartao;
import negocio.domain.CartaoResidente;
import negocio.domain.Dinheiro;
import park.domain.Parquimetro;

/**
 *
 * @author Weirich
 */
public class PagamentoDAODerby implements PagamentoDAO {
    
    public int selectUltimoPagamento() throws PagamentoDAOException{
          String sql = "select max(id_pagamento) as id from pagamento";
          int val = 1;  
          try (Connection conexao = InitDB.conectarBd()) {
                try (PreparedStatement comando = conexao.prepareStatement(sql)) {
                    try (ResultSet resultado = comando.executeQuery()) {
                        
                        while (resultado.next()) {
                            val = resultado.getInt("id");
                        }
                    }
                }
            } catch (Exception e) {
                throw new PagamentoDAOException();
            }
            return val;
    }
    
     public void inserirPagamento(double valor, Date data_pgto,String id_pagamento, String num_cartao, String tipo) {
     String sql = "insert into pagamento(valor, data_pgto) values(?, ?)";
    try (Connection conexao = InitDB.conectarBd()) {
       try (PreparedStatement comando = conexao.prepareStatement(sql)) {
                comando.setDouble(1, valor);
                comando.setDate(2, (java.sql.Date) data_pgto);
                if (comando.executeUpdate() > 0) {
                    System.out.println("Inserção efetuada com sucesso");
                } else {
                    System.out.println("Falha na inserção");
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    
    if(tipo.equals("cartao")){
    sql = "insert into cartao(id_pagamento, num_cartao) values(?, ?)";
    try (Connection conexao = InitDB.conectarBd()) {
       try (PreparedStatement comando = conexao.prepareStatement(sql)) {
                comando.setInt(1, selectUltimoPagamento());
                comando.setString(2, num_cartao);
                if (comando.executeUpdate() > 0) {
                    System.out.println("Inserção efetuada com sucesso");
                } else {
                    System.out.println("Falha na inserção");
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    else{
    sql = "insert into cartao_residente(id_pagamento, num_cartao) values(?, ?)";
    try (Connection conexao = InitDB.conectarBd()) {
       try (PreparedStatement comando = conexao.prepareStatement(sql)) {
                comando.setString(1, id_pagamento);
                comando.setString(2, num_cartao);
                if (comando.executeUpdate() > 0) {
                    System.out.println("Inserção efetuada com sucesso");
                } else {
                    System.out.println("Falha na inserção");
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    }
    
    @Override
    public List<Pagamento> buscarTodos() throws PagamentoDAOException {
        List<Pagamento> listaPagamentos = new ArrayList<>();
        String sql = "select pagamento.id_pagamento, cartao.num_cartao, pagamento.valor, pagamento.data_pgto from cartao inner join pagamento on cartao.id_pagamento = pagamento.id_pagamento ";
        try (Connection conexao = InitDB.conectarBd()) {
            try (PreparedStatement comando = conexao.prepareStatement(sql)) {
                try (ResultSet resultado = comando.executeQuery()) {
                    while (resultado.next()) {
                        int id_pagamento = resultado.getInt("id_pagamento");
                        String num_cartao = resultado.getString("num_cartao");
                        Double valor = resultado.getDouble("valor");
                        Date dataHora = resultado.getDate("data_pgto");

                        listaPagamentos.add(new Cartao(id_pagamento, dataHora));
                    }
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(PagamentoDAODerby.class.getName()).log(Level.SEVERE, null, ex);
        }
        sql = "select pagamento.id_pagamento, cartao_residente.num_cartao, pagamento.valor, pagamento.data_pgto from cartao_residente "
                + "inner join pagamento on cartao_residente.id_pagamento = pagamento.id_pagamento ";
        try (Connection conexao = InitDB.conectarBd()) {
            try (PreparedStatement comando = conexao.prepareStatement(sql)) {
                try (ResultSet resultado = comando.executeQuery()) {
                    while (resultado.next()) {
                        int id_pagamento = resultado.getInt("id_pagamento");
                        String num_cartao = resultado.getString("num_cartao");
                        Float valor = resultado.getFloat("valor");
                        Date dataHora = resultado.getDate("data_pgto");

                        listaPagamentos.add(new CartaoResidente(valor, dataHora));
                    }
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(PagamentoDAODerby.class.getName()).log(Level.SEVERE, null, ex);
        }

        sql = "select pagamento.id_pagamento, pagamento.valor, pagamento.data_pgto from dinheiro"
                + " inner join pagamento on dinheiro.id_pagamento = pagamento.id_pagamento ";
        try (Connection conexao = InitDB.conectarBd()) {
            try (PreparedStatement comando = conexao.prepareStatement(sql)) {
                try (ResultSet resultado = comando.executeQuery()) {
                    while (resultado.next()) {
                        int id_pagamento = resultado.getInt("id_pagamento");
                        Float valor = resultado.getFloat("valor");
                        Date dataHora = resultado.getDate("data_pgto");

                        listaPagamentos.add(new Dinheiro(valor, dataHora));
                    }
                }
            }
        } catch (Exception e) {
            throw new PagamentoDAOException();
        }
        return listaPagamentos;
    }

    
    public Float buscaTotalIsento(Date inicio, Date fim) throws PagamentoDAOException {
        float valorIsento = 0.0f;
        String sql = "select sum(pagamento.valor) as valor from cartao_residente inner join pagamento on cartao_residente.id_pagamento = pagamento.id_pagamento where "
                + "pagamento.data_pgto between  ? and ? ";
        try (Connection conexao = InitDB.conectarBd()) {
            try (PreparedStatement comando = conexao.prepareStatement(sql)) {
                comando.setDate(1, new java.sql.Date(inicio.getTime()));
                comando.setDate(2, new java.sql.Date(fim.getTime()));
                try (ResultSet resultado = comando.executeQuery()) {
                    while (resultado.next()) {
                        valorIsento = resultado.getFloat("valor");
                    }
                }
            }
        } catch (Exception e) {
            throw new PagamentoDAOException();
        }
        return valorIsento;
    }

    @Override
    public Float buscaTotalArrecadado(Date inicio, Date fim) throws PagamentoDAOException {
        Float valorArrecadado  = 0.0f;
        String sql = "select sum(pagamento.valor) as valor from pagamento left join cartao on cartao.id_pagamento = pagamento.id_pagamento left join dinheiro on pagamento.id_pagamento = dinheiro.id_pagamento where pagamento.data_pgto between ? and ?";
        try (Connection conexao = InitDB.conectarBd()) {
            try (PreparedStatement comando = conexao.prepareStatement(sql)) {
                comando.setDate(1, new java.sql.Date(inicio.getTime()));
                comando.setDate(2, new java.sql.Date(fim.getTime()));
                try (ResultSet resultado = comando.executeQuery()) {
                    while (resultado.next()) {
                        valorArrecadado = resultado.getFloat("valor");
                    }
                }
            }
        } catch (Exception e) {
            throw new PagamentoDAOException();
        }
        return valorArrecadado;
    }

    /**
     *
     * @param idParquimetro
     * @param agrupStr
     * @param valores
     * @param data
     * @throws PagamentoDAOException
     */
    @Override
    public void buscaPagos(String idParquimetro, String agrupStr, List<Float> valores, List<String> data) throws PagamentoDAOException {
        int idPark = Integer.parseInt(idParquimetro);
        TiposAgrupamento agrup;
        if (agrupStr.equalsIgnoreCase("mes")) {
            agrup = TiposAgrupamento.AGRUP_MES;
        } else if (agrupStr.equalsIgnoreCase("ano")) {
            agrup = TiposAgrupamento.AGRUP_ANO;
        } else {
            agrup = TiposAgrupamento.AGRUP_DIA;
        }
        if (agrup.equals(agrup.AGRUP_MES)) {
            String sql = "select sum(pagamento.valor) as soma, month(pagamento.data_pgto) as mes from pagamento "
                    + "left join cartao on cartao.id_pagamento = pagamento.id_pagamento "
                    + "left join dinheiro on dinheiro.id_pagamento = pagamento.id_pagamento "
                    + "inner join tickets on tickets.id_pagamento = pagamento.id_pagamento "
                    + "inner join parquimetros on tickets.id_parquimetro = parquimetros.id_parquimetro "
                    + "where parquimetros.id_parquimetro = ?"
                    + "group by month(pagamento.data_pgto)";
            try (Connection conexao = InitDB.conectarBd()) {
                try (PreparedStatement comando = conexao.prepareStatement(sql)) {
                    comando.setInt(1, idPark);
                    try (ResultSet resultado = comando.executeQuery()) {
                        while (resultado.next()) {
                            Float valor = resultado.getFloat("soma");
                            String mes = resultado.getString("mes");
                            valores.add(valor);
                            data.add(mes);
                        }
                    }
                }
            } catch (Exception e) {
                throw new PagamentoDAOException();
            }
        } else if (agrup.equals(agrup.AGRUP_DIA)) {
            String sql = "select sum(pagamento.valor) as soma, day(pagamento.data_pgto) as dia from pagamento "
                   + "left join cartao on cartao.id_pagamento = pagamento.id_pagamento "
                    + "left join dinheiro on dinheiro.id_pagamento = pagamento.id_pagamento "
                    + "inner join tickets on tickets.id_pagamento = pagamento.id_pagamento "
                    + "inner join parquimetros on tickets.id_parquimetro = parquimetros.id_parquimetro "
                    + "where parquimetros.id_parquimetro = ?"
                    + "group by day(pagamento.data_pgto)";
            try (Connection conexao = InitDB.conectarBd()) {
                try (PreparedStatement comando = conexao.prepareStatement(sql)) {
                    comando.setInt(1, idPark);
                    try (ResultSet resultado = comando.executeQuery()) {
                        while (resultado.next()) {
                            Float valor = resultado.getFloat("soma");
                            String dia = resultado.getString("dia");
                            valores.add(valor);
                            data.add(dia);

                            valores.add(valor);
                        }
                    }
                }
            } catch (Exception e) {
                throw new PagamentoDAOException();
            }
        } else {
            String sql = "select sum(pagamento.valor) as soma, year(pagamento.data_pgto) as ano from pagamento "
                    + "left join cartao on cartao.id_pagamento = pagamento.id_pagamento "
                    + "left join dinheiro on dinheiro.id_pagamento = pagamento.id_pagamento "
                    + "inner join tickets on tickets.id_pagamento = pagamento.id_pagamento "
                    + "inner join parquimetros on tickets.id_parquimetro = parquimetros.id_parquimetro "
                    + "where parquimetros.id_parquimetro = ?"
                    + "group by year(pagamento.data_pgto)";
            try (Connection conexao = InitDB.conectarBd()) {
                try (PreparedStatement comando = conexao.prepareStatement(sql)) {
                    comando.setInt(1, idPark);
                    try (ResultSet resultado = comando.executeQuery()) {
                        while (resultado.next()) {
                            Float valor = resultado.getFloat("soma");
                            String ano = resultado.getString("ano");
                            data.add(ano);

                            valores.add(valor);
                        }
                    }
                }
            } catch (Exception e) {
                throw new PagamentoDAOException();
            }

        }
    }

    @Override
    public Pagamento buscarPorCodigo(int codigo) throws PagamentoDAOException {
        Pagamento pagamento = null;
        String sql = "select pagamento.id_pagamento, cartao.num_cartao, pagamento.valor, pagamento.data_pgto from cartao inner join pagamento on cartao.id_pagamento = pagamento.id_pagamento "
                + "where cartao.id_pagamento = ?";
        try (Connection conexao = InitDB.conectarBd()) {
            try (PreparedStatement comando = conexao.prepareStatement(sql)) {
                comando.setInt(1, codigo);
                try (ResultSet resultado = comando.executeQuery()) {
                    while (resultado.next()) {
                        int id_pagamento = resultado.getInt("id_pagamento");
                        String num_cartao = resultado.getString("num_cartao");
                        Double valor = resultado.getDouble("valor");
                        Date dataHora = resultado.getDate("data_pgto");

                        pagamento = new Cartao(id_pagamento, dataHora);
                    }
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(PagamentoDAODerby.class.getName()).log(Level.SEVERE, null, ex);
        }
        sql = "select pagamento.id_pagamento, cartao_residente.num_cartao, pagamento.valor, pagamento.data_pgto from cartao_residente "
                + "inner join pagamento on cartao_residente.id_pagamento = pagamento.id_pagamento where cartao_residente.id_pagamento = ?";
        try (Connection conexao = InitDB.conectarBd()) {
            try (PreparedStatement comando = conexao.prepareStatement(sql)) {
                comando.setInt(1, codigo);
                try (ResultSet resultado = comando.executeQuery()) {
                    while (resultado.next()) {
                        int id_pagamento = resultado.getInt("id_pagamento");
                        String num_cartao = resultado.getString("num_cartao");
                        Float valor = resultado.getFloat("valor");
                        Date dataHora = resultado.getDate("data_pgto");
                        pagamento = new CartaoResidente(valor, dataHora);
                    }
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(PagamentoDAODerby.class.getName()).log(Level.SEVERE, null, ex);
        }

        sql = "select pagamento.id_pagamento, pagamento.valor, pagamento.data_pgto from dinheiro"
                + " inner join pagamento on dinheiro.id_pagamento = pagamento.id_pagamento where dinheiro.id_pagamento = ?";
        try (Connection conexao = InitDB.conectarBd()) {
            try (PreparedStatement comando = conexao.prepareStatement(sql)) {
                comando.setInt(1, codigo);
                try (ResultSet resultado = comando.executeQuery()) {
                    while (resultado.next()) {
                        int id_pagamento = resultado.getInt("id_pagamento");
                        float valor = resultado.getFloat("valor");
                        Date dataHora = resultado.getDate("data_pgto");
                        pagamento = new Dinheiro(valor, dataHora);
                    }
                }
            }
        } catch (Exception e) {
            throw new PagamentoDAOException();
        }

        return pagamento;
    }

    @Override
    public void buscaIsentos(String idParquimetro, String agrupStr, List<Float> valores, List<String> datas) throws PagamentoDAOException {
        int idPark = Integer.parseInt(idParquimetro);
        TiposAgrupamento agrup;
        if (agrupStr.equalsIgnoreCase("mes")) {
            agrup = TiposAgrupamento.AGRUP_MES;
        } else if (agrupStr.equalsIgnoreCase("ano")) {
            agrup = TiposAgrupamento.AGRUP_ANO;
        } else {
            agrup = TiposAgrupamento.AGRUP_DIA;
        }
        if (agrup.equals(agrup.AGRUP_MES)) {
            String sql = "select sum(pagamento.valor) as soma, month(pagamento.data_pgto) as mes from pagamento inner join cartao_residente on cartao_residente.id_pagamento = pagamento.id_pagamento inner join tickets on tickets.id_pagamento = pagamento.id_pagamento inner join parquimetros on tickets.id_parquimetro = parquimetros.id_parquimetro where parquimetros.id_parquimetro = ? group by month(pagamento.data_pgto)";
            try (Connection conexao = InitDB.conectarBd()) {
                try (PreparedStatement comando = conexao.prepareStatement(sql)) {
                    comando.setInt(1, idPark);
                    try (ResultSet resultado = comando.executeQuery()) {
                        while (resultado.next()) {
                            Float valor = resultado.getFloat("soma");
                            String mes = resultado.getString("mes");
                            valores.add(valor);
                            datas.add(mes);
                        }
                    }
                }
            } catch (Exception e) {
                throw new PagamentoDAOException();
            }
        } else if (agrup.equals(agrup.AGRUP_DIA)) {
            String sql = "select sum(pagamento.valor) as soma, day(pagamento.data_pgto) as dia from pagamento inner join cartao_residente on cartao_residente.id_pagamento = pagamento.id_pagamento inner join tickets on tickets.id_pagamento = pagamento.id_pagamento inner join parquimetros on tickets.id_parquimetro = parquimetros.id_parquimetro where parquimetros.id_parquimetro = ? group by day(pagamento.data_pgto)";
            try (Connection conexao = InitDB.conectarBd()) {
                try (PreparedStatement comando = conexao.prepareStatement(sql)) {
                    comando.setInt(1, idPark);
                    try (ResultSet resultado = comando.executeQuery()) {
                        while (resultado.next()) {
                            Float valor = resultado.getFloat("soma");
                            String dia = resultado.getString("dia");
                            valores.add(valor);
                            datas.add(dia);
                        }
                    }
                }
            } catch (Exception e) {
                throw new PagamentoDAOException();
            }
        } else {
            String sql = "select sum(pagamento.valor) as soma, year(pagamento.data_pgto) as ano from pagamento" +
"                    inner join cartao_residente on cartao_residente.id_pagamento = pagamento.id_pagamento" +
"                    inner join tickets on tickets.id_pagamento = pagamento.id_pagamento" +
"                    inner join parquimetros on tickets.id_parquimetro = parquimetros.id_parquimetro" +
"                    where parquimetros.id_parquimetro = ?" +
"                    group by year(pagamento.data_pgto)";
            try (Connection conexao = InitDB.conectarBd()) {
                try (PreparedStatement comando = conexao.prepareStatement(sql)) {
                   comando.setInt(1, idPark);
                    try (ResultSet resultado = comando.executeQuery()) {
                        while (resultado.next()) {
                            Float valor = resultado.getFloat("soma");
                            String ano = resultado.getString("ano");
                            valores.add(valor);
                            datas.add(ano);
                        }
                    }
                }
            } catch (Exception e) {
                throw new PagamentoDAOException();
            }

        }

    }
}
