/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistencia;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import negocio.dao.InitDB;
import negocio.dao.PagamentoDAOException;
import negocio.dao.ParquimetroDAO;
import negocio.dao.ParquimetroDAOException;
import negocio.dao.TicketDAOException;
import park.domain.Pagamento;
import park.domain.Parquimetro;


/**
 *
 * @author Weirich
 */
public class ParquimetroDAODerby implements ParquimetroDAO {

 
    
     public void inserirParquimetro(String endereco , String horarioIni , String horarioFim, int tempoMin, int tempoMax, int incremento, float valorIncremento) {
     String sql = "insert into parquimetros(endereco, horario_ini_tarifacao, horario_fim_tarifacao, tempo_minimo, tempo_maximo, incremento, valor_incremento) values(?, ?, ?, ?, ?,?, ? )";
    try (Connection conexao = InitDB.conectarBd()) {
       try (PreparedStatement comando = conexao.prepareStatement(sql)) {
                comando.setString(1, endereco);
                comando.setString(2, horarioIni);
                comando.setString(3, horarioFim);
                comando.setInt(4, tempoMin);
                comando.setInt(5, tempoMax);
                comando.setInt(6, incremento);
                comando.setFloat(7, valorIncremento);
                
                if (comando.executeUpdate() > 0) {
                    System.out.println("Inserção efetuada com sucesso");
                } else {
                    System.out.println("Falha na inserção");
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    
    }
    
    @Override
    public List<Parquimetro> buscarTodos() throws ParquimetroDAOException {
             List<Parquimetro> listaParquimetros = new ArrayList<>();
        String sql = "select * from parquimetros "; 
        try (Connection conexao = InitDB.conectarBd()) {
            try (PreparedStatement comando = conexao.prepareStatement(sql)) {
                try (ResultSet resultado = comando.executeQuery()) {
                    while (resultado.next()) {
                        int id_parquimetro = resultado.getInt("id_parquimetro");
                        String endereco = resultado.getString("endereco");
                        String horario_ini_tarifacao = resultado.getString("horario_ini_tarifacao");
                        String horario_fim_tarifacao = resultado.getString("horario_fim_tarifacao");
                        int tempo_minimo = resultado.getInt("tempo_minimo");
                        int tempo_maximo = resultado.getInt("tempo_maximo");  
                        int incremento = resultado.getInt("incremento");    
                        float valor_incremento = resultado.getFloat("valor_incremento");    


                        listaParquimetros.add(new Parquimetro(id_parquimetro, endereco, horario_ini_tarifacao, horario_fim_tarifacao, tempo_minimo, tempo_maximo, valor_incremento, incremento));
                    }
                    return listaParquimetros;
                }
            }
        } catch (Exception e) {
            throw new ParquimetroDAOException();
        }
    }

    @Override
    public Parquimetro buscarPorCodigo(int codigo) throws ParquimetroDAOException {
        Parquimetro parq = null; 
        String sql = "select * from parquimetros where id_parquimetro = ? "; 
        try (Connection conexao = InitDB.conectarBd()) {
            try (PreparedStatement comando = conexao.prepareStatement(sql)) {
                comando.setInt(1, codigo);
                try (ResultSet resultado = comando.executeQuery()) {
                    while(resultado.next()){
                        int id_parquimetro = resultado.getInt("id_parquimetro");
                        String endereco = resultado.getString("endereco");
                        String horario_ini_tarifacao = resultado.getString("horario_ini_tarifacao");
                        String horario_fim_tarifacao = resultado.getString("horario_fim_tarifacao");
                        int tempo_minimo = resultado.getInt("tempo_minimo");
                        int tempo_maximo = resultado.getInt("tempo_maximo");  
                        int incremento = resultado.getInt("incremento");    
                        float valor_incremento = resultado.getFloat("valor_incremento");    

                        parq = new Parquimetro(id_parquimetro, endereco, horario_ini_tarifacao, horario_fim_tarifacao, tempo_minimo, tempo_maximo, valor_incremento, incremento);
                    }
                    return parq;
                }
            }
        } catch (Exception e) {
            throw new ParquimetroDAOException();
        }
    }

    @Override
    public float totalArrecadado(int codigo) throws ParquimetroDAOException {
       float valorTotal =0;
        String sql = "select sum(pagamento.valor)from parquimetros inner join tickets on tickets.id_parquimetro = parquimetro.id_parquimetro "
               + "inner join pagamento on tickets.id_pagamento = pagamento.id_pagamento where parquimetro.id_parquimetro = ?"; 
        try (Connection conexao = InitDB.conectarBd()) {
            try (PreparedStatement comando = conexao.prepareStatement(sql)) {
                comando.setInt(1, codigo);
                try (ResultSet resultado = comando.executeQuery()) {
                       valorTotal =  resultado.getFloat(1);
                }
            } return valorTotal;
        } catch (Exception e) {
            throw new ParquimetroDAOException();
        }
    }

    @Override
    public void inserir(Parquimetro parquimetro) throws ParquimetroDAOException {
         String sql = "insert into parquimetros(id_parquimetro,endereco, horario_ini_tarifacao, horario_fim_tarifacao, tempo_minimo, tempo_maximo, incremento, valor_incremento)]"
                 + " values (?,?,?,?,?,?,?,?)";
        try (Connection conexao = InitDB.conectarBd()) {
            try (PreparedStatement comando = conexao.prepareStatement(sql)) {
                comando.setInt(1, parquimetro.getIdentificacao());
                comando.setString(2, parquimetro.getEndereco());
                comando.setString(3, parquimetro.getHorarioIniTarifacao());
                comando.setString(4, parquimetro.getHorarioFimTarifacao());
                comando.setInt(5, parquimetro.getTempoMinimo());
                comando.setInt(6, parquimetro.getTempoMaximo());
                comando.setInt(7, parquimetro.getIncremento());
                comando.setDouble(8, parquimetro.getValorIncremento());
                int status = comando.executeUpdate();
            }
        } catch (Exception e) {
            throw new ParquimetroDAOException();
        }
    }

    @Override
    public void alterar(Parquimetro parquimetro) throws ParquimetroDAOException {
       String sql = "update table parquimetros set endereco = ?, horario_ini_tarifacao = ? , horario_fim_tarifacao = ? , tempo_minimo = ?, tempo_maximo = ?, incremento = ?, valor_incremento = ?  where id_parquimetro = "+ parquimetro.getIdentificacao(); 
        try (Connection conexao = InitDB.conectarBd()) {
                try (PreparedStatement comando = conexao.prepareStatement(sql)) {
                comando.setInt(1, parquimetro.getIdentificacao());
                comando.setString(2, parquimetro.getEndereco());
                comando.setString(3, parquimetro.getHorarioIniTarifacao());
                comando.setString(4, parquimetro.getHorarioFimTarifacao());
                comando.setInt(5, parquimetro.getTempoMinimo());
                comando.setInt(6, parquimetro.getTempoMaximo());
                comando.setInt(7, parquimetro.getIncremento());
                comando.setDouble(8, parquimetro.getValorIncremento());
                int status = comando.executeUpdate();       
            } 
        } catch (Exception e) {
            throw new ParquimetroDAOException();
        }
    }

    @Override
    public List<Float> totalArrecadadoAno(int codigo) throws ParquimetroDAOException {
        List<Float> listaTotaisPorAno = new ArrayList<>();
        String sql = "select sum(pagamento.valor)from parquimetros inner join tickets on tickets.id_parquimetro = parquimetro.id_parquimetro "
               + "inner join pagamento on tickets.id_pagamento = pagamento.id_pagamento where parquimetro.id_parquimetro = ? group by tickets.year(data_hora_emissao) "; 
        try (Connection conexao = InitDB.conectarBd()) {
            try (PreparedStatement comando = conexao.prepareStatement(sql)) {
                comando.setInt(1, codigo);
                try (ResultSet resultado = comando.executeQuery()) {
                       while(resultado.next()){
                           listaTotaisPorAno.add(resultado.getFloat(1));
                       }
                }
            } return listaTotaisPorAno;
        } catch (Exception e) {
            throw new ParquimetroDAOException();
        }
    }
    
}
