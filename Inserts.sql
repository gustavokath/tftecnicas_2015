insert into parquimetros(endereco, horario_ini_tarifacao, horario_fim_tarifacao, tempo_minimo, tempo_maximo, incremento, valor_incremento) values('rua abc', '12:30', '14:30', 5, 100,2, 5 );
insert into parquimetros(endereco, horario_ini_tarifacao, horario_fim_tarifacao, tempo_minimo, tempo_maximo, incremento, valor_incremento) values('rua def', '11:00', '14:30', 40, 100,2, 5 );
insert into parquimetros(endereco, horario_ini_tarifacao, horario_fim_tarifacao, tempo_minimo, tempo_maximo, incremento, valor_incremento) values('rua ghi', '10:30', '16:30', 60, 100,2, 5 );
insert into parquimetros(endereco, horario_ini_tarifacao, horario_fim_tarifacao, tempo_minimo, tempo_maximo, incremento, valor_incremento) values('rua jkll ', '15:30', '18:30', 60, 100,1, 10 );
insert into parquimetros(endereco, horario_ini_tarifacao, horario_fim_tarifacao, tempo_minimo, tempo_maximo, incremento, valor_incremento) values('rua ghi', '20:30', '22:30', 6, 30,3, 12 );

insert into tickets(data_hora_emissao,data_hora_validade, id_parquimetro, id_pagamento) values('2014-01-28','2014-02-28',1,1);
insert into tickets(data_hora_emissao,data_hora_validade, id_parquimetro, id_pagamento) values('2014-01-28','2014-02-28',1,2);
insert into tickets(data_hora_emissao,data_hora_validade, id_parquimetro, id_pagamento) values('2014-01-28','2014-02-28',2,1);
insert into tickets(data_hora_emissao,data_hora_validade, id_parquimetro, id_pagamento) values('2014-01-28','2014-02-28',2,3);
insert into tickets(data_hora_emissao,data_hora_validade, id_parquimetro, id_pagamento) values('2014-01-28','2014-02-28',1,3);

insert into pagamento(valor, data_pgto) values(3, '2000-01-22');
insert into pagamento(valor, data_pgto) values(2.5, '2014-02-28');
insert into pagamento(valor, data_pgto) values(1.75, '2003-05-27');
insert into pagamento(valor, data_pgto) values(1.00, '1990-04-23');
insert into pagamento(valor, data_pgto) values(0.75, '1899-02-21');
insert into pagamento(valor, data_pgto) values(1.25, '1999-04-26');
insert into pagamento(valor, data_pgto) values(2.5, '1956-03-05');
insert into pagamento(valor, data_pgto) values(3, '2000-01-26');
insert into pagamento(valor, data_pgto) values(1.00, '1999-02-10');

insert into dinheiro(id_pagamento) values(1);
insert into dinheiro(id_pagamento) values(2);
insert into dinheiro(id_pagamento) values(3);

insert into cartao(id_pagamento, num_cartao) values(4, '1234567888');
insert into cartao(id_pagamento, num_cartao) values(5, '1234577778');
insert into cartao(id_pagamento, num_cartao) values(6, '12345789878');

insert into cartao_residente(id_pagamento, num_cartao) values(7, '7777323');
insert into cartao_residente(id_pagamento, num_cartao) values(8, '723466678');
insert into cartao_residente(id_pagamento, num_cartao) values(9, '133929993');
